/**
 * This service download pacient notifications from Erinle server
 * and save it to a SharedPreferences.
 */

package org.colivre.erinle.memo;

import java.text.ParseException;
import java.util.Date;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.os.PersistableBundle;
import android.support.v4.app.NotificationCompat;
import android.util.ArraySet;
import android.content.Context;
import android.util.Log;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class GetMessagesService extends JobService {
    private static final String DTAG = "ErinleDebug";
    private static Context context;
    public static final String CONF_NAME = "MemoConfFile";
    public static final String MESSAGES_MEMO = "MessagesMemoFile";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(DTAG, "GetMessages created");
        GetMessagesService.context = getApplicationContext();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(DTAG, "GetMessages destroyed");
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        notifyAlive("requisitando...");
        Log.d(DTAG, "+Start GetMessages " + params.getJobId() + " -- " + (new java.util.Date()).toString());
        SharedPreferences conf = getSharedPreferences(MainActivity.CONF_NAME, 0);
        new DownloadJSON().execute(conf.getString("pacientKey", ""));

        // Return true as there's more work to be done with this job.
        return false;
    }

    @Override
    public boolean onStopJob(final JobParameters params) {
        Log.d(DTAG, "-Stop GetMessages " + params.getJobId() + " -- " + (new java.util.Date()).toString());
        return false;
    }

    private static void notifyAlive(String lastReqUpdateResult) {
        Context ctx = GetMessagesService.context;
        int myID = 1;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx);
        builder.setContentTitle("Memo está ativo. ");
        builder.setContentText(lastReqUpdateResult +" "+ (new Date()).toString());
        builder.setSmallIcon(R.drawable.pills);
        builder.setColorized(true);
        builder.setColor(0x0099DD);
        builder.setOngoing(true);
        Notification notification = builder.build();
        NotificationManager nm = (NotificationManager) ctx.getSystemService(NOTIFICATION_SERVICE);
        nm.notify(myID, notification);
    }

    private static void closeAllCurrentNotificationJobs() {
        JobScheduler tm = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        SharedPreferences memo = context.getSharedPreferences(MESSAGES_MEMO, 0);
        for (Map.Entry<String, ?> pair : memo.getAll().entrySet()) {
            int msgID = Integer.parseInt(pair.getKey());
            tm.cancel(msgID);
            nm.cancel(msgID);
        }
        SharedPreferences.Editor editor = context.getSharedPreferences(MESSAGES_MEMO, 0).edit();
        editor.clear();
        editor.commit();
    }

    public static void updateMessages(String[] data) {
        closeAllCurrentNotificationJobs();
        notifyAlive(data[0]);
        try {
            JSONObject root = new JSONObject(data[1]);
            if (root.getBoolean("ok")) {
                JSONArray messages = root.getJSONArray("messages");
                for (int i = 0; i < messages.length(); i++) {
                    registerMessage(messages.getJSONObject(i));
                }
            } else {
                Log.d(DTAG, "Someting is wrong. JSON is not ok.");
            }
            updateNotifications();
        } catch (JSONException ex) {
            Log.e(DTAG, "JSON root parse fail! " + ex.getMessage());
        }
    }

    public static void registerMessage(JSONObject message) {
        SharedPreferences memo = context.getSharedPreferences(MESSAGES_MEMO, 0);
        SharedPreferences.Editor editor = memo.edit();
        String msgID = "";
        String msgRunAt = "";
        String msgMonitoring = "";
        String msgTitle = "";
        String msgDescription = "";
        String notifiableType = "";
        try {
            msgID = Integer.toString(message.getInt("id"));
            msgRunAt = message.getString("run_at");
            msgMonitoring = message.getString("monitoring");
            notifiableType = message.getString("notifiable_type");
            if (notifiableType.equals("Dosage")) {
                JSONObject dosage = message.getJSONObject("dosage");
                msgTitle = Integer.toString(dosage.getInt("amount")) +" "+
                           dosage.getString("amount_unit") +" de "+
                           dosage.getString("drug_name");
                msgDescription = dosage.getString("description");
            } else if (notifiableType.equals("Appointment")) {
                String[] text = message.getString("text").split("\n");
                msgTitle = text[0];
                msgDescription = text[1];
            } else {
                msgTitle = "Lembrete de Saúde";
                msgDescription = message.getString("text");
            }
        } catch (org.json.JSONException ex) {
            Log.e(DTAG, "JSON parse fail! " + ex.getMessage());
        } finally {
            Set data = new ArraySet();
            Set default_data = new ArraySet();
            default_data.add("mon:no_answer");
            Log.d(DTAG, "Load "+msgID+": "+memo.getStringSet(msgID, default_data).toString());
            if(memo.getStringSet(msgID, default_data).contains("mon:no_answer")) {
                Log.d(DTAG, "Register or Update message "+msgID+".");
                data.add("rAt:"+msgRunAt);
                data.add("mon:"+msgMonitoring);
                data.add("tit:"+msgTitle);
                data.add("des:"+msgDescription);
                data.add("typ:"+notifiableType);
                editor.putStringSet(msgID, data);
                editor.commit();
            } else {
                Log.d(DTAG, "Message "+msgID+" answered.");
            }
        }
    }

    public static void updateNotifications() {
        Log.d(DTAG, "update...");
        SharedPreferences memo = context.getSharedPreferences(MESSAGES_MEMO, 0);
        for (Map.Entry<String, ?> pair : memo.getAll().entrySet()) {
            int msgID = Integer.parseInt(pair.getKey());
            if ( ( (Set<String>)pair.getValue() ).contains("mon:no_answer") ) {
                scheduleNotificationJob( msgID, memoValToMap((Set<String>) pair.getValue()) );
            } else {
                Log.d(DTAG, "The message "+pair.getKey()+" is done.");
            }
        }
    }

    public static Map<String, String> memoValToMap(Set<String> value) {
        String[] arr = value.toArray(new String[0]);
        Map<String, String> valMap = new HashMap<String, String>();
        for (int i=0; i<arr.length; i++) {
            String v = arr[i].toString();
            String k = v.substring(0,3);
            v = v.substring(4);
            valMap.put(k, v);
        }
        return valMap;
    }

    public static void scheduleNotificationJob(int msgID, Map<String, String> data) {
        Log.d(DTAG, data.get("rAt") + " " + data.get("mon") + " " + data.get("tit"));
        ComponentName mServiceComponent = new ComponentName(context, NotificationJob.class);
        JobInfo.Builder builder = new JobInfo.Builder(msgID, mServiceComponent);

        builder.setPeriodic(30*1000);
        builder.setPersisted(true);

        try {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            PersistableBundle extras = new PersistableBundle();
            extras.putInt("msgID", msgID);
            extras.putLong("runAt", parser.parse(data.get("rAt")).getTime());
            extras.putString("title", data.get("tit"));
            extras.putString("description", data.get("des"));
            extras.putString("notifiableType",data.get("typ"));
            builder.setExtras(extras);

            // Schedule job
            Log.d(DTAG, "Scheduling Notification job...");
            JobScheduler tm = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            tm.schedule(builder.build());

            String pacientKey = context.getSharedPreferences(MainActivity.CONF_NAME, 0).getString("pacientKey", "");
            String msgIdStr = Integer.toString(msgID);
            new UpdateMessage().execute(pacientKey, msgIdStr, "status", "completed");
        } catch (ParseException ex) {
            Log.d(DTAG, "FUUUUUUUUUUUUUU!!! "+ex.getMessage());
        }
    }

}

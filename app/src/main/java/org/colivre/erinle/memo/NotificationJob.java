package org.colivre.erinle.memo;

import java.util.Set;
import android.util.ArraySet;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.os.PersistableBundle;
import java.util.Date;

public class NotificationJob extends JobService {
    private static final String DTAG = "ErinleDebug";
    private static final String ACTION_DONE = "erinle.memo.action.DONE";
    private static final String MESSAGES_MEMO = "MessagesMemoFile";
    private static final String CLOSED_MSGS_MEMO = "ClosedMessagesMemoFile";
    private int msgID;
    private Long runAt;
    private String title;
    private String description;
    private String notifiableType;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(DTAG, "NotificationJob created");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(DTAG, "NotificationJob destroyed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(DTAG, "onStartCommand");
        if ( intent.getAction() == ACTION_DONE ) {
            String pacientKey = getSharedPreferences(MainActivity.CONF_NAME, 0).getString("pacientKey", "");
            int msgID = intent.getIntExtra("id", 0);
            String msgIdStr = Integer.toString(msgID);
            boolean done = intent.getBooleanExtra("done", false);
            Log.d(DTAG, "Did the notified task " + msgID + " → " + done);
            String monitStatus = done ? "done" : "not_done";
            new UpdateMessage().execute(pacientKey, msgIdStr, "monitoring", monitStatus);
            close(msgID, monitStatus);
        }
        else {
            Log.d(DTAG, "Não sei o que é " + intent.getAction());
        }
        return START_NOT_STICKY;
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        Log.d(DTAG, "+Start NotificationJob " + params.getJobId() + " -- " + (new java.util.Date()).toString());
        Long oneHour = 60*60*1000L;
        Date now = new Date();
        PersistableBundle extras = params.getExtras();
        this.msgID = extras.getInt("msgID");
        String msgIdStr = Integer.toString(this.msgID);
        this.runAt = extras.getLong("runAt");
        this.title = extras.getString("title");
        this.description = extras.getString("description");
        this.notifiableType = extras.getString("notifiableType");
        if ( now.after(new Date(this.runAt)) ) {
            if (now.after(new Date(this.runAt + oneHour))) {
                Log.d(DTAG, "Msg " + msgIdStr + " is too old. Canceling...");
                close(this.msgID, "canceled");
            } else {
                createNotification();
            }
        } else {
            Log.d(DTAG, Integer.toString(this.msgID)+" run in future.");
        }

        // Return true as there's more work to be done with this job.
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        // Stop tracking these job parameters, as we've 'finished' executing.
        Log.d(DTAG, "-Stop NotificationJob " + params.getJobId());

        // Return false to drop the job.
        return false;
    }

    private void close(int msgID, String monitStatus) {
        SharedPreferences memo = getSharedPreferences(MESSAGES_MEMO, 0);
        SharedPreferences closedMemo = getSharedPreferences(CLOSED_MSGS_MEMO, 0);
        SharedPreferences.Editor editor = memo.edit();
        SharedPreferences.Editor closedEditor = closedMemo.edit();
        String msgIdStr = Integer.toString(msgID);

        try {
            Set<String> dataSet = new ArraySet();
            dataSet.addAll(memo.getStringSet(msgIdStr, java.util.Collections.EMPTY_SET));
            dataSet.remove("mon:no_answer");
            dataSet.add("mon:" + monitStatus);

            editor.remove(msgIdStr);
            closedEditor.putStringSet(msgIdStr, dataSet);
            Log.d(DTAG, "CLOSE "+msgIdStr+": "+dataSet.toString());
        } catch (Exception ex) {
            Log.d(DTAG, "ArraySet Fail! "+ ex.toString() +" --- "+ ex.getMessage());
        }

        editor.apply();
        closedEditor.apply();

        JobScheduler tm = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        tm.cancel(msgID);
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(msgID);
    }

    private void createNotification() {
        Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.coin);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Date runAt = new Date(this.runAt);
        String time = (new java.text.SimpleDateFormat("HH:mm")).format(runAt);
        builder.setContentTitle(this.title);
        if (this.notifiableType.equals("Dosage")) {
            builder.setContentText(time + " → " + this.description);
            builder.setTicker(time + " " + this.title + ". " + this.description);
        } else {
            builder.setContentText(this.description);
            builder.setTicker(this.title + ". " + this.description);
        }
        builder.setSmallIcon(R.drawable.pills);
        builder.setColorized(true);
        builder.setColor(0xDD0000);
        builder.setAutoCancel(false);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setSound(soundUri);
        builder.setLights(0xFFDD00, 200, 300);
        builder.setVibrate(new long[] {1000, 500, 1000});
        builder.setOngoing(true);

        /// Buttons ////////////////////////////////////////////////////////////////////////////////
        ComponentName comp = new ComponentName(getPackageName(), NotificationJob.class.getName());
        int fUpdate = PendingIntent.FLAG_UPDATE_CURRENT;

        if (this.notifiableType.equals("Dosage")) {
            Intent intentDone = new Intent(ACTION_DONE).setComponent(comp)
                    .putExtra("id", this.msgID)
                    .putExtra("done", true);
            int reqDone = this.msgID * 2;
            PendingIntent pendingDone = PendingIntent.getService(this, reqDone, intentDone, fUpdate);
            builder.addAction(R.drawable.thumbs_up, "Tomei!", pendingDone);

            Intent intentNotDone = new Intent(ACTION_DONE).setComponent(comp)
                    .putExtra("id", this.msgID)
                    .putExtra("done", false);
            int reqNotDone = this.msgID * 2 + 1; // ensure reqDone and reqNotDone are different.
            PendingIntent pendingNotDone = PendingIntent.getService(this, reqNotDone, intentNotDone, fUpdate);
            builder.addAction(R.drawable.thumbs_down, "Não tomei", pendingNotDone);
        } else {
            Intent intentDone = new Intent(ACTION_DONE).setComponent(comp)
                    .putExtra("id", this.msgID)
                    .putExtra("done", true);
            int reqDone = this.msgID * 2;
            PendingIntent pendingDone = PendingIntent.getService(this, reqDone, intentDone, fUpdate);
            builder.addAction(R.drawable.thumbs_up, "Ok", pendingDone);
        }

        Notification notification = builder.build();
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(this.msgID, notification);
    }

}

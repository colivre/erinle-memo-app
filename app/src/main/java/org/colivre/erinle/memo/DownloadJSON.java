package org.colivre.erinle.memo;

import android.os.AsyncTask;
import android.util.Log;

import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import android.os.NetworkOnMainThreadException;
import java.net.URL;

public class DownloadJSON extends AsyncTask<String, Integer, String[]> {

    private static final String DTAG = "ErinleDebug";
    //public static final String CONF_NAME = "MemoConfFile";

    @Override
    protected String[] doInBackground(String... inputs) {
        String lastReqUpdateResult = "sem resposta.";
        String pacientKey = inputs[0];
        Log.e(DTAG, "Request updates for " + pacientKey);
        StringBuilder respBody = new StringBuilder();
        try {
            URL url = new URL("https://memo.ufba.br/api/v1/message.json?pacientkey=" + pacientKey);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30*1000);
            conn.setConnectTimeout(30*1000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int statusCode = conn.getResponseCode();
            Log.d(DTAG, "URLConnection statusCode: "+statusCode);
            Log.d(DTAG, "URLConnection response Msg: "+conn.getResponseMessage());
            lastReqUpdateResult = Integer.toString(statusCode) + " " + conn.getResponseMessage();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) respBody.append(inputLine);
            in.close();
            conn.disconnect();
        } catch (MalformedURLException ex) {
            Log.e(DTAG, "Erinle URL Fail! " + ex.getMessage());
            lastReqUpdateResult = ex.getMessage();
        } catch (IOException ex) {
            Log.e(DTAG, "Network IO Fail! " + ex.getMessage());
            // lastReqUpdateResult got statusCode.
        } catch(NetworkOnMainThreadException ex) {
            Log.e(DTAG, "Network thread Fail! " + ex.getMessage());
            lastReqUpdateResult = ex.getMessage();
        }
        String[] data = {lastReqUpdateResult, respBody.toString()};
        return data;
    }

    protected void onPostExecute(String[] data) {
        GetMessagesService.updateMessages(data);
    }
}
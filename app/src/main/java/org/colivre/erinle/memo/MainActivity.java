package org.colivre.erinle.memo;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String DTAG = "ErinleDebug";
    public static final String CONF_NAME = "MemoConfFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView keyLabel = (TextView) findViewById(R.id.keyLabel);
        keyLabel.setText(getPacientKey());

        initGetMessages();
    }

    public String getPacientKey() {
        SharedPreferences conf = getSharedPreferences(CONF_NAME, 0);
        String pacientKey = conf.getString("pacientKey", null);
        if (pacientKey == null) {
            pacientKey = Double.toHexString(Math.random()).split("[.p]")[1];
            SharedPreferences.Editor editor = conf.edit();
            editor.putString("pacientKey", pacientKey);
            editor.commit();
        }
        return pacientKey;
    }

    public void initGetMessages() {
        ComponentName mServiceComponent = new ComponentName(this, GetMessagesService.class);
        JobInfo.Builder builder = new JobInfo.Builder(1, mServiceComponent);

        builder.setPeriodic(120*1000);//60*60*1000
        builder.setPersisted(true);

        // Schedule job
        Log.d(DTAG, "Start GetMessages job.");
        JobScheduler tm = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        tm.schedule(builder.build());
    }

}

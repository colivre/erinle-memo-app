package org.colivre.erinle.memo;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import android.os.NetworkOnMainThreadException;
import java.net.URL;


public class UpdateMessage extends AsyncTask<String, Integer, Integer> {
    private static final String DTAG = "ErinleDebug";

    @Override
    protected Integer doInBackground(String... inputs) {
        String pacientKey = inputs[0];
        String msgIdStr   = inputs[1];
        String param      = inputs[2];
        String value      = inputs[3];
        String tries      = "0";
        if (inputs.length > 4) tries = inputs[4];
        int statusCode    = 0;
        Log.d(DTAG,"Update msg "+msgIdStr+" ("+param+"="+value+") try: "+tries);
        try {
            URL url = new URL("https://memo.ufba.br/api/v1/message/" + msgIdStr + ".json" +
                              "?pacientkey=" + pacientKey +
                              "&"+ param +"=" + value);
            //Log.d(DTAG, url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30*1000);
            conn.setConnectTimeout(30*1000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            statusCode = conn.getResponseCode();
            conn.disconnect();
        } catch (MalformedURLException ex) {
            Log.e(DTAG, "Erinle URL Fail! " + ex.getMessage());
        } catch (IOException ex) {
            Log.e(DTAG, "Network IO Fail! " + ex.getMessage());
        } catch(NetworkOnMainThreadException ex) {
            Log.e(DTAG, "Network thread Fail! " + ex.getMessage());
        }
        if ( statusCode<200 && statusCode>299 )
            this.retryUpdate(pacientKey, msgIdStr, param, value, tries);
        return statusCode;
    }

    protected void onPostExecute(Integer statusCode) {
        Log.d(DTAG, "Update Message statusCode: " + statusCode);
    }

    protected void retryUpdate(String pacientKey, String msgIdStr, String param, String value, String triesStr) {
        int triesInt = Integer.parseInt(triesStr) + 1;
        triesStr = Integer.toString(triesInt);
        if (triesInt < 9) {
            try {
                this.wait(triesInt * 2 * 60 * 1000);
            } catch (InterruptedException ex) {
                Log.e(DTAG, "Wait Fail! "+ex.getMessage());
            }
            new UpdateMessage().execute(pacientKey, msgIdStr, param, value, triesStr);
        }
    }
}
